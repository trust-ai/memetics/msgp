#!/usr/bin/env python

from setuptools import setup, find_packages
import msgp

setup(
    name='msgp',
    version=msgp.__version__,
    description='Memetic semantic genetic programming (GP) for symbolic regression',
    long_description=open('README.md').read(),
    author=msgp.__author__,
    author_email='msgp@inria.fr',
    platforms=['any'],
    license='MIT',
    keywords=['memetic evolutionary algorithm', 'memetic algorithm'],
    url='https://gitlab.inria.fr/trust-ai/meme/msgp',
    packages=find_packages(exclude=['msgp.datasets']),
    install_requires=[
        'pandas==1.4.2',
        'numpy==1.22.4',
        'deap==1.3.3',
        'scikit-learn==1.1.1',
        'tqdm==4.62.3',
        'networkx==2.7.1',
        'graphviz==0.17',
        'matplotlib==3.5.0',
        'mictools==1.1.4'
    ],
)
