import sys

from typing import List

from numpy import ndarray

import numpy as np
import pandas as pd

from deap import creator, base
from deap import gp
from deap import tools

from msgp.tools import protected_division, \
    get_subtrees, compute_node_outputs, linear_scaling, evaluate_tree, aq
from . import deapfix


class Library(list):

    def __init__(self,
                 pop_size: int,
                 min_height: int,
                 max_height: int,
                 generation_function,
                 feature_names: List[str], *args):

        super().__init__(self, *args)

        self.pop_size = pop_size
        self.min_height = min_height
        self.max_height = max_height
        self.generation_function = generation_function
        self.feature_names = feature_names
        self.pset = self.create_primitive_set()
        self.toolbox = self.create_toolbox()

        # self.individuals = self.toolbox.population(n=self.pop_size)
        individuals = dict()
        for individual in self.toolbox.population(n=self.pop_size):
            subtrees = get_subtrees(individual, self.toolbox)
            for _, _, _, node in subtrees:
                individuals[str(node)] = node

            # individuals.extend([tree for _, _, _, tree in subtrees])
            # individuals.append(individual)

        self.extend(individuals.values())

        # self.hof = tools.HallOfFame(self.pop_size)
        # self.stats = self.create_stats()

    def create_primitive_set(self) -> gp.PrimitiveSetTyped:
        pset = gp.PrimitiveSetTyped("library", [np.ndarray] * len(self.feature_names), np.ndarray)

        pset.addPrimitive(np.add, [np.ndarray, np.ndarray], np.ndarray, "add")
        pset.addPrimitive(np.subtract, [np.ndarray, ndarray], np.ndarray, "sub")
        pset.addPrimitive(np.multiply, [np.ndarray, np.ndarray], np.ndarray, name="mul")
        # pset.addPrimitive(protected_division, [np.ndarray, np.ndarray], np.ndarray, name="div")
        pset.addPrimitive(aq, [np.ndarray, np.ndarray], np.ndarray, name="div")

        # To respect half-and-half generation
        # pset.addEphemeralConstant("RandomTuple", (lambda: np.random.uniform(-1, 1)), np.ndarray)

        args = dict()
        if self.feature_names is not None:
            for i, name in enumerate(self.feature_names):
                args["ARG" + str(i)] = name
        pset.renameArguments(**args)

        return pset

    def create_toolbox(self) -> base.Toolbox:

        try:
            creator.FitnessMin
        except:
            creator.create("FitnessMin", base.Fitness, weights=(-1.0,))

        try:
            creator.Individual
        except:
            creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMin)

        toolbox = base.Toolbox()
        # Ramped Half and half generation (full and grow)
        toolbox.register("expr", self.generation_function, pset=self.pset, min_=self.min_height, max_=self.max_height)
        # toolbox.register("expr", gp.genHalfAndHalf, pset=self.pset, min_=2, max_=5)
        toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr)
        toolbox.register("population", tools.initRepeat, list, toolbox.individual)
        toolbox.register("compile", gp.compile, pset=self.pset)

        return toolbox

    # def create_stats(self) -> tools.MultiStatistics:
    #
    #     stats_fit = tools.Statistics(lambda ind: ind.fitness.values)
    #     stats_size = tools.Statistics(len)
    #
    #     stats = tools.MultiStatistics(fitness=stats_fit, size=stats_size)
    #     stats.register("avg", np.mean)
    #     stats.register("std", np.std)
    #     stats.register("min", np.min)
    #     stats.register("max", np.max)
    #
    #     return stats

    def terminal_names(self, expr) -> List[str]:
        args = []
        for i, node in enumerate(expr):
            if isinstance(node, gp.Terminal) and not (isinstance(node, gp.Ephemeral)):
                args.append(node.value)
        return list(set(args))  # set to avoid duplicated name


    def compile(self, expr, context):
        code = str(expr)
        names = self.terminal_names(expr)

        if len(names) > 0:
            args = ",".join(arg for arg in names)
            code = "lambda {args}: {code}".format(args=args, code=code)

        try:
            return eval(code, context, {})
        except MemoryError:
            _, _, traceback = sys.exc_info()
            raise MemoryError("DEAP : Error in tree evaluation :"
                              " Python cannot evaluate a tree higher than 90. "
                              "To avoid this problem, you should use bloat control on your "
                              "operators. See the DEAP documentation for more information. "
                              "DEAP will now abort.").with_traceback(traceback)

    def predict_ind(self, individual, data) -> np.ndarray:

        func = self.compile(expr=individual, context=self.pset.context)
        names = self.terminal_names(expr=individual)

        args = dict()
        for name in names:
            feature_index = self.feature_names.index(name)
            # if feature_index > data.shape[1]:
            #     continue
            args[name] = data[:, feature_index]

        if len(names) > 0:
            y_pred = func(**args)
        else:
            y_pred = [func]

        return np.asarray(y_pred)

    def fitness_function(self, individual, data, y_true, verbose: bool = __debug__):
        y_pred = self.predict_ind(individual, data)
        # mse = mean_squared_error(y_true, ([y_pred] * y_true.shape[0]) if y_pred.shape[0] == 1 else y_pred)
        _, _, mse = linear_scaling(y_true, y_pred)
        # Deap requires multiple values be returned, so the comma is important!
        return mse,

    def fit(self, X, y, verbose: bool = __debug__) -> None:
        self.toolbox.register("evaluate", self.fitness_function, data=X, y_true=y, verbose=verbose)
        self.toolbox.register("individual_prediction", self.predict_ind, data=X)

        trees = []

        for ind in self:
            # node_id, parent_id, slice(start, end), and subtree
            subtrees = get_subtrees(ind, self.toolbox)
            compute_node_outputs(subtrees, self.toolbox)
            new_subtree = subtrees[0][3]
            evaluate_tree([new_subtree], self.toolbox)

            if np.isnan(new_subtree.fitness.values[0]):
                continue

            added = False
            for i, tree in enumerate(trees):
                if np.alltrue(np.equal(new_subtree.outputs, tree.outputs)):
                    if new_subtree.height < tree.height:
                        trees[i] = new_subtree
                        added = True
                        break

            if not added:
                trees.append(new_subtree)

        self.clear()
        self.extend(trees)

        for i_ind, ind in enumerate(self):
            ind.id = i_ind

        # self.hof.update(self)

    def dump_trees(self, filename) -> None:
        libraries = [str(tree) for tree in self]
        errors = [tree.fitness.values[0] if len(tree.fitness.values) > 0 else 0. for tree in self]

        df = pd.DataFrame(data=list(zip(libraries, errors)),
                          columns=['expression', 'fitness'])
        df.to_csv(filename, index=False, sep="\t")



