from collections import defaultdict

class IndexBanRules(object):
    """This class records the master tree index positions at which
    archtrees are inserted.
    """

    def __init__(self):
        self.rules = defaultdict(set)

    def add_rule(self, i_node, banned_i_archtree):
        """Add a archtree to the ban list for an index postion i_node."""
        self.rules[i_node].add(banned_i_archtree)

    def set_master_tree_str(self, master_tree):
        """This is here for consistency with StateBanRules."""
        pass

    def get_avoid_indexes(self, i_node):
        """Returns a list of archtree indexes which can not be inserted
        at the i_node master_tree index position.
        """
        return self.rules[i_node]