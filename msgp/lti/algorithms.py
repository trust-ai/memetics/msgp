import random
import time

from math import ceil

from copy import deepcopy
from typing import List, Sequence, Tuple, Dict

import numpy as np
import numpy.random
from deap import base, creator, gp
from deap.tools import Logbook
from sklearn.utils import Bunch

from msgp.lti import library
from msgp.lti import ban
from msgp import tools

from tqdm import tqdm


def is_ancestor(node_id, node_q, nodes: Dict[(id, Bunch)]) -> bool:

    """
    Check if a tree's node is ancestor of another one
    :param node_id: node to check if it's ancestor of the query node
    :param node_q: node to check its origin
    :param nodes: dictionary with the nodes of the tree
    :return: True if node_id is the ancestor of node_q
    """

    parent_id = nodes[node_q].parent_id

    if node_id == parent_id:
        return True
    else:
        return is_ancestor(parent_id, node_q, nodes)


def error(b: gp.PrimitiveTree, a: gp.PrimitiveTree) -> float:
    # We follow the definition proposed by Ffrancon et al., 2015 (Equation 5)
    # that is different from the one used by sklearn
    return tools.local_error(b.outputs, a.optimal)


def weighted_choice(choices):
    """Given a list of ordered selections c and associated
    selection probabilities w, this function selects and returns
    one of the choices at random.

    Arguments:
    choices -- a list of (c, w) elements
    c -- object returned if element selected
    w -- probability of selecting element.
    """
    total = sum(w for c, w in choices)
    r = random.uniform(0, total)
    upto = 0
    for c, w in choices:
        if upto + w > r:
            return c
        upto += w


def rank_selection(nodes: Dict[int, gp.PrimitiveTree]) -> (id, gp.PrimitiveTree):
    # probs = np.zeros(len(nodes))
    probs = []
    prob = len(nodes)

    for i, node in enumerate(nodes.values()):
        if i == 0:
            probs.append(prob)
            continue

        node_1 = list(nodes.values())[i - 1]
        if node.error != node_1.error and node.height != node_1.error:
            prob -= 1
            probs.append(prob)
            continue

        probs.append(prob)

    choices = []
    for node, prob in zip(nodes.values(), probs):
        choices.append((node, prob))
    choices.reverse()

    node = weighted_choice(choices)
    return node


def lti(subtrees: List[Tuple[int, int, Sequence, gp.PrimitiveTree]],
        nodes: List[Bunch], lib_: library.Library, ban_rules: ban.IndexBanRules,
        toolbox: base.Toolbox,
        verbose: bool = __debug__):

    nodes_dict = dict((node_id, [node, parent_id, slice_])
                      for node_id, parent_id, slice_, node in subtrees)
    tree = nodes_dict[0]

    tau = [(b.id, b.node) for b in nodes]
    tau = {k: v for k, v in sorted(tau, key=lambda item: item[1].error)}
    tau = {k: v for k, v in sorted(tau.items(), key=lambda item: item[1].height, reverse=True)}

    logbook = Logbook()

    best_ = dict()
    node_a = rank_selection(tau)
    min_error = node_a.error

    avoid_i_arch = ban_rules.rules[node_a.id]
    best_[node_a.id] = []

    for i_arch_tree, B in enumerate(lib_):

        if i_arch_tree in avoid_i_arch:
            continue

        if error(B, node_a) == 0:
            best_[node_a.id] = [(B, 0)]
            break

        if error(B, node_a) < min_error:
            best_[node_a.id] = [(B, error(B, node_a))]
            min_error = error(B, node_a)

        elif error(B, node_a) == min_error:
            best_[node_a.id].append((B, min_error))

        if len(best_[node_a.id]) > 0:
            (B, error_b) = best_[node_a.id][0]
            _, parent_id, slice_ = nodes_dict[node_a.id]

            if node_a.error - error_b > 0.:
                if verbose:
                    logbook.record(node_a={"name": str(node_a), "error": node_a.error},
                                   node_b={"name": str(B), "error": error_b})
                    print()
                    print(logbook)

                ban_rules.add_rule(node_a.id, B.id)

                new_tree = [creator.Individual(crossover(slice_, B, tree))]
                return new_tree

    node_a = random.choice(nodes)
    node_b = random.choice(lib_)
    new_tree = [creator.Individual(crossover(node_a.slice_, node_b, tree))]

    ban_rules.add_rule(node_a.id, node_b.id)
    return new_tree


def iterated_lti(individual,
                 lib_: library.Library,
                 context,
                 toolbox: base.Toolbox,
                 pset,
                 min_height,
                 max_height,
                 y,
                 epochs: int,
                 max_time: int = 300,
                 stopping_criteria: float = 0.001,
                 disable_progressbar: bool = False,
                 boosting: bool = False,
                 residuals: bool = False,
                 num_samples: int = None,
                 verbose: bool = __debug__) -> List[Bunch]:

    trees = []
    trees_i = []
    tree_i = individual

    ban_rules = ban.IndexBanRules()
    tree = deepcopy(individual)

    y = deepcopy(y)

    if num_samples is None:
        num_samples = int(ceil(len(y)) / 2)

    start_time = time.time()
    current_time = 0

    logbook = Logbook()
    for i in tqdm(range(1, epochs + 1), disable=disable_progressbar):

        # Generate a Rademacher random vector with the variables (+1, -1).
        random_rademacher_vector = np.random.randint(low=2, size=(num_samples, len(y))) * 2 - 1

        # if isinstance(tree[0].root, creator.Individual):
        #     break
        subtrees = tools.get_subtrees(tree[0], toolbox)

        tools.compute_node_outputs(subtrees, toolbox)
        nodes = tools.compute_node_optimal_values(subtrees, context, y)
        nodes_dict = dict((b.id, b) for b in nodes)

        tools.evaluate_tree([nodes_dict[0].node], toolbox)
        # the starting tree may be semantically incorrect
        if not np.isnan(nodes_dict[0].node.fitness.values[0]):
            rademacher_complexity = tools.compute_rademacher_complexity(predictions=nodes_dict[0].node.predictions,
                                                                        rademacher_vector=random_rademacher_vector,
                                                                        num_samples=num_samples)
            nodes_dict[0].node.rademacher_complexity = rademacher_complexity
            trees.append(Bunch(epoch=i,
                               tree=nodes_dict[0].node,
                               height=nodes_dict[0].node.height,
                               length=len(nodes_dict[0].node),
                               fitness=nodes_dict[0].node.fitness.values[0],
                               error=nodes_dict[0].node.error,
                               residuals=nodes_dict[0].node.residuals,
                               coefficients=nodes_dict[0].node.coefficients,
                               rademacher_complexity=nodes_dict[0].node.rademacher_complexity))
            if boosting or residuals:
                y = np.subtract(y, nodes_dict[0].node.residuals)

        if nodes_dict[0].node.fitness.values[0] <= stopping_criteria and \
           current_time > max_time:
            break

        lti_tree = lti(subtrees, nodes, lib_, ban_rules, toolbox, verbose=verbose)
        if isinstance(lti_tree[0].root, creator.Individual):
            lti_tree = [lti_tree[0].root]

        # mutant = toolbox.clone(lti_tree)
        # new_tree = deap.gp.mutNodeReplacement(mutant[0], pset=pset)
        new_tree = lti_tree

        tools.evaluate_tree(new_tree, toolbox)
        if np.isnan(new_tree[0].fitness.values[0]):
            if verbose:
                print(f"\nIgnoring individual {new_tree[0]} since it leads to incorrect results")
            continue

        if tree_i[0].fitness.values[0] > new_tree[0].fitness.values[0]:
            trees_i.append((i, tree_i))
            tree_i = new_tree

        if min_height > new_tree[0].height or max_height < new_tree[0].height:
            if verbose:
                print(f"\nIgnoring individual {new_tree[0]} due to its height {new_tree[0].height} condition")
            continue

        tree = new_tree

        if verbose:
            if isinstance(tree[0].root, creator.Individual):
                break

            tools.evaluate_tree(tree, toolbox)
            logbook.record(trees={"a": {"expr": str(nodes_dict[0].node), "error": nodes_dict[0].node.fitness.values[0],
                                        "height": nodes_dict[0].node.height},
                                  "b": {"expr": str(tree[0]), "error": tree[0].fitness.values[0],
                                        "height": tree[0].height}})
            print()
            print(logbook)

        current_time = time.time() - start_time
        if current_time > max_time:
            break

    end_time = time.time()
    time_taken = end_time - start_time

    trees = sorted(trees, key=lambda item: item.height)
    trees = sorted(trees, key=lambda item: item.error)

    return trees


def crossover(slice_: Sequence, new_subtree, tree) -> List[gp.PrimitiveTree]:

    if not isinstance(slice_.start, int):
        return tree

    t = tree[0][0:slice_.start] + new_subtree + tree[0][slice_.stop:]
    return t


def best(tree_a: gp.PrimitiveTree, tree_b: gp.PrimitiveTree) -> gp.PrimitiveTree:
    if tree_a[0].fitness.values[0] < tree_b[0].fitness.values[0]:
        return tree_a
    elif tree_a[0].fitness.values[0] == tree_b[0].fitness.values[0]:
        return tree_a if tree_a[0].height <= tree_b[0].height else tree_b

    return tree_b


def improve(new_node: gp.PrimitiveTree, current_node: (id, gp.PrimitiveTree),
            nodes: List[Tuple[int, int, Sequence, gp.PrimitiveTree]], greedy: bool) -> bool:

    current_node_id, current_node_ = current_node

    error_ = tools.local_error(new_node.outputs, current_node_.optimal)
    if current_node_.local_error - error_ > 0:
        if greedy:
            dct = dict((node_id, (parent_id, node)) for node_id, parent_id, _, node in nodes)
            parent_id, _ = dct[current_node_id]
            if parent_id is not None:
                _, parent_node = dct[parent_id]
                return improve(new_node, (parent_id, parent_node), nodes)
        #     else:
        #         return True
        # else:
        #     return True
        return True

    return False
