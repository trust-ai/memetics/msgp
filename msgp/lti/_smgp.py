import os
import random
from typing import List

import numpy as np
import pandas as pd
from deap import creator, base
from deap import gp
from deap import tools as gptools
from numpy import ndarray
from sklearn.metrics import explained_variance_score, mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.utils import Bunch

from msgp import tools
from msgp.lti import deapfix
from . import algorithms as algo
from . import library as lib


class SemanticRegressor(object):

    def __init__(self,
                 name: str = "MAIN",
                 stopping_criteria: float = 0.001,
                 generation_function: str = 'HalfAndHalf',
                 n_best: int = 5,
                 feature_names: List[str] = None,
                 min_height=2,
                 max_height=4,
                 epochs: int = 100,
                 use_posterior: bool = False,
                 random_state: int = 42):

        self.name = name
        self.stopping_criteria = stopping_criteria
        self.generation_function = generation_function
        self.n_best = n_best
        self.feature_names = feature_names
        self.min_height = min_height
        self.max_height = max_height

        self.epochs = epochs
        self.random_state = random_state

        self._library = None
        self._start_individual = None
        self._trees = None

        self._best_trees = None

        self._use_posterior = use_posterior
        self._posterior = None

        random.seed(self.random_state)
        np.random.seed(self.random_state)

        self._pset = self.__create_pset_typed__()
        self._toolbox = self.__create_toolbox__()

        self._tree_feature_names = None
        self._tree = None
        self._trees = None
        self._best_trees = None

    def __create_pset_typed__(self) -> gp.PrimitiveSetTyped:
        pset = gp.PrimitiveSetTyped(self.name, [ndarray] * len(self.feature_names), ndarray)

        pset.addPrimitive(np.add, [ndarray, ndarray], ndarray, "add")
        pset.addPrimitive(np.subtract, [ndarray, ndarray], ndarray, "sub")
        pset.addPrimitive(np.multiply, [ndarray, ndarray], ndarray, name="mul")
        # pset.addPrimitive(tools.protected_division, [ndarray, ndarray], ndarray, name="div")
        pset.addPrimitive(tools.aq, [ndarray, ndarray], ndarray, name="div")

        args = dict()
        if self.feature_names is not None:
            for i, name in enumerate(self.feature_names):
                args["ARG" + str(i)] = name
        pset.renameArguments(**args)

        return pset

    def predict_ind(self, individual, data):

        func = tools.gp_compile(expr=individual,
                                context=self._pset.context)
        names = tools.terminal_names(expr=individual)

        args = dict()
        for name in names:
            args[name] = data[:, self.feature_names.index(name)]

        if len(names) > 0:
            y_pred = func(**args)
        else:
            y_pred = [func]

        return np.asarray(y_pred)

    def fitness_function(self, individual, data, y_true, verbose: bool = __debug__):
        y_pred = self.predict_ind(individual, data)
        sigma = np.var(y_pred)
        residuals_ = y_true - y_pred

        if 10e-7 > sigma > 10e7:
            if verbose:
                print(f"Ignoring individual ({str(individual)}) due to its outputs' variance ({sigma})")
            return np.nan,

        if self._use_posterior:
            from msgp.posteriors import linear
            x_posterior = pd.DataFrame(data, columns=self.feature_names)[tools.terminal_names(expr=individual)].values
            posterior = linear(np.c_[x_posterior, y_true], residuals_)
            mu, sigma = posterior(np.c_[x_posterior, y_pred])
            # self._posterior = linear(np.c_[x_posterior, yhat], residuals)
            y_pred += mu

        a, b, mse = tools.linear_scaling(y_true,  y_pred)
        individual.coefficients = [a, b]

        individual.predictions = None
        individual.residuals = None

        if a is not None and b is not None:
            individual.predictions = a + b * y_pred
            individual.residuals = y_true - individual.predictions

        individual.predictions_ = y_pred
        individual.residuals_ = residuals_

        # Deap requires multiple values be returned, so the comma is important!
        return mse,

    def fit(self, X, y, **args):

        verbose = args['verbose'] if 'verbose' in args else False
        validation_size = args['validation_size'] if 'validation_size' in args else 0.25

        X_train, X_val, y_train, y_val = train_test_split(X, y,
                                                          test_size=validation_size,
                                                          random_state=self.random_state)
        self._toolbox.register("evaluate",
                               self.fitness_function,
                               data=X_train,
                               y_true=y_train,
                               verbose=verbose)

        self._toolbox.register("individual_prediction",
                               self.predict_ind,
                               data=X_train)

        library = lib.Library(pop_size=args['population_size'],
                              min_height=args['min_height'],
                              max_height=args['max_height'],
                              generation_function=deapfix.get_generation_strategy(self.generation_function),
                              feature_names=self.feature_names)
        library.fit(X_train, y_train, verbose=verbose)
        self._library = library

        warm_start = args['warm_start'] if 'warm_start' in args else False

        individual = self._toolbox.population(n=1) if not warm_start else library.hof
        tools.evaluate_tree(individual, self._toolbox)
        self._start_individual = individual

        disable_progressbar = args['disable_progressbar'] if 'disable_progressbar' in args else False

        trees = algo.iterated_lti(individual=individual,
                                  lib_=library,
                                  context=self._pset.context,
                                  toolbox=self._toolbox,
                                  pset=self._pset,
                                  min_height=self.min_height,
                                  max_height=self.max_height,
                                  y=y_train,
                                  epochs=self.epochs,
                                  disable_progressbar=disable_progressbar,
                                  verbose=verbose)

        best_tree = None
        for b in trees:
            y_pred = b.tree.coefficients[0] + b.tree.coefficients[1] * self.predict_ind(b.tree, X_val)
            b.fitness_val = mean_squared_error(y_val, y_pred)
            b.explained_variance_score = explained_variance_score(y_val, y_pred)
            best_tree = b if best_tree is None or best_tree.fitness_val > b.fitness_val else best_tree

        unique_trees_dict = dict((str(b.tree), b if not np.isnan(b.fitness_val) else None) for b in trees)
        unique_trees = [unique_trees_dict[k] for k in unique_trees_dict.keys()]
        unique_trees = list(filter(lambda tree: tree is not None, unique_trees))

        sorted_unique_trees = sorted(unique_trees, key=lambda tree: tree.fitness_val)
        sorted_unique_trees = sorted(sorted_unique_trees, key=lambda tree: tree.height)

        self._trees = trees
        self._best_trees = sorted_unique_trees
        self._tree = self._best_trees[0]
        self._tree_feature_names = tools.terminal_names(self._tree.tree)

        # if self._use_posterior:
        #     # from gstools import Exponential, Gaussian
        #     # from gstools.krige import Ordinary
        #
        #     from msgp.posteriors import linear
        #
        #     tree_posterior = self._tree if not self.boosting else self.boosting_tree
        #     yhat = tree_posterior.tree.coefficients[0] + \
        #            self.predict_ind(tree_posterior.tree, X) * \
        #            tree_posterior.tree.coefficients[1]
        #     residuals = (y - yhat)
        #
        #     x_posterior = pd.DataFrame(data=X, columns=self.feature_names)[self._tree_feature_names].values
        #     # self._posterior = linear(np.c_[x_posterior, yhat], residuals)
        #     self._posterior = linear(np.c_[x_posterior, y], residuals)
        #     # model = Exponential(dim=x_posterior.shape[1] + 1, var=np.var(residuals))
        #     # self._posterior = Ordinary(model=model, cond_pos=np.c_[x_posterior, yhat],
        #     #                            cond_val=residuals)

        return self

    def dump_results(self, **kwargs) -> None:

        if self._tree is None:
            return

        import matplotlib.pyplot as plt

        workdir = kwargs['workdir'] if 'workdir' in kwargs else False

        if not os.path.exists(workdir):
            os.makedirs(workdir, exist_ok=False)

        trees = sorted(self._trees, key=lambda item: item.epoch)

        epochs = [tree.epoch for tree in trees]
        errors = [tree.tree.fitness.values[0] for tree in trees]
        test_errors = [tree.fitness_test for tree in trees]
        local_errors = [tree.error for tree in trees]
        heights = [tree.height for tree in trees]
        expressions = [str(tree.tree) for tree in trees]
        lengths = [tree.length for tree in trees]
        complexities = [tree.rademacher_complexity for tree in trees]

        fig, ax = tools.plot(errors, xlabel='epochs', ylabel='Loss')
        fig.savefig(f'{workdir}/loss.pdf')

        _, _ = tools.plot(heights, xlabel='epochs', ylabel='Height',
                          title="Tree's height", filename=f'{workdir}/tree_heights.pdf')

        df = pd.DataFrame(data=list(zip(epochs, expressions, errors, local_errors,
                                        heights, lengths, test_errors, complexities)),
                          columns=['epochs', 'expression', 'error', 'local error',
                                   'height', 'length', 'test_error', 'rademacher complexity'])
        df.to_csv(f"{workdir}/individuals.csv",
                  index=False,
                  sep="\t")

        self._library.dump_trees(f"{workdir}/libraries.csv")

        graphs_viz = [f"{workdir}/start_tree.pdf", f"{workdir}/fitness_tree.pdf"]
        for filename, tree in list(zip(graphs_viz, [self._start_individual, [self._tree]])):
            tree_ = tree[0].tree if isinstance(tree[0], Bunch) else tree[0]
            fig, ax = plt.subplots(figsize=(12, 12))
            _ = tools.plot_tree(tree_,
                                ax,
                                filename=f"{workdir}/{filename.split('/')[-1].split('.')[0]}.dot")
            title = f"Tree:{str(tree_)}\nMSE:{np.round(tree_.fitness.values[0], 2)}"
            ax.set_title(title, fontsize=6)
            fig.savefig(filename)

    def predict(self, data):
        if self._tree is None:
            raise Exception("You must call fit before predict!")

        predictions = self._tree.tree.coefficients[0] + \
                      self.predict_ind(self._tree.tree, data) * \
                      self._tree.tree.coefficients[1]

        # if self._use_posterior and self._posterior is not None:
        #     x_posterior = pd.DataFrame(data=data, columns=self.feature_names)[self._tree_feature_names].values
        #     mu_e, var_e = self._posterior(np.c_[x_posterior, predictions])
        #     # for i in range(0, len(var_e)):
        #     #     if np.sqrt(var_e[i]) > self.stopping_criteria:
        #     #         mu_e[i] = 0.0
        #     predictions += mu_e
        return predictions

    def __create_toolbox__(self) -> base.Toolbox:
        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
        creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMin, outputs=np.array, optimals=np.array)

        toolbox = base.Toolbox()
        # Ramped Half and half generation (full and grow)
        g_function = deapfix.get_generation_strategy(self.generation_function)

        toolbox.register("expr", g_function, pset=self._pset, min_=self.min_height, max_=self.max_height)
        # toolbox.register("expr", gp.genHalfAndHalf, pset=self.pset, min_=2, max_=5)
        toolbox.register("individual", gptools.initIterate, creator.Individual, toolbox.expr)
        toolbox.register("population", gptools.initRepeat, list, toolbox.individual)
        toolbox.register("compile", gp.compile, pset=self._pset)

        return toolbox

    def get_tree(self):
        return self._tree

    def get_tree_expression(self):
        return self.get_expression(simplify=True)

    def get_best_trees(self):
        return self._best_trees

    def get_trees(self):
        return self._trees

    def get_expression(self, simplify: bool = False) -> str:
        return tools.get_tree_expression(self.get_tree(), simplify)

    def get_start_individual(self):
        return self._start_individual

    def get_pset(self) -> gp.PrimitiveSetTyped:
        return self._pset

    def get_toolbox(self) -> base.Toolbox:
        return self._toolbox

    def close(self):

        self._toolbox.unregister("individual")
        self._toolbox.unregister("population")
        self._toolbox.unregister("expr")

        # del creator.FitnessMin
        # del creator.Individual

        del self._pset
        del self._toolbox

    # def __del__(self):
    #     del creator.FitnessMin
    #     del creator.Individual
