from ._smgp import SemanticRegressor

__all__ = [
    "SemanticRegressor",
]
