import sys
from copy import deepcopy
from functools import partial

import autograd.numpy as np
from autograd import grad
from deap import gp
from sklearn.utils import shuffle

from msgp import tools


def compile(code, context, arguments):
    if len(arguments) > 0:
        args = ",".join(arg for arg in arguments)
        code = "lambda {args}: {code}".format(args=args, code=code)
    try:
        return eval(code, context, {})
    except MemoryError:
        _, _, traceback = sys.exc_info()
        raise MemoryError("Gradient Descent : Error in tree evaluation :"
                          " Python cannot evaluate a tree higher than 90. "
                          "To avoid this problem, you should use bloat control on your "
                          "operators. See the DEAP documentation for more information. "
                          "DEAP will now abort.", traceback)


def one_example_loss(pair, tree, filters, args):
    import numpy
    x, y = pair
    # real_class = 1. if y == classes[0] else 0.

    # out = tree(x, *filters)[0]
    y_pred = tree(**args)[0]

    # predicted_class = helpers.sigmoid(out)  # Pass through a sigmoid, since treating as probability value
    # loss = helpers.ce_loss(real_class, predicted_class)
    # y_pred = x * out
    loss = numpy.sum((y - y_pred) ** 2) / len(x)
    return loss


def compute_loss(tree, xs, ys, filters, args):
    length = len(xs)

    partial_func = partial(one_example_loss, tree=tree, filters=filters, args=args)

    losses = map(partial_func, zip(xs, ys))
    total_loss = sum(losses)

    # Average loss so batch size doesn't effect magnitude
    return total_loss / float(length)


def split_and_shuffle(x, y, split=0.8):
    x, y = shuffle(x, y)
    length = int(len(x) * split)

    return x[:length], y[:length], x[length:], y[length:]


# Creates a generator of batches
def make_batches(x, y, batch_size):
    length = len(x)
    for i in range(length // batch_size):
        yield x[batch_size * i:batch_size * (i + 1)], y[batch_size * i:batch_size * (i + 1)]


# Replace all filters with placeholders - So we can pass them in as arguments for computing derivatives easily
def replace_filters_with_args(tree_str, labels, context, arguments):
    filters = {}  # Keep a copy of the original filters
    prefix = "Filter"

    for node, label in labels.items():
        # Filters are represented as lists, so we only care for lists
        if type(label) == float:
            arg_name = prefix + str(node)
            filters[node] = label  # Save the original value
            labels[node] = arg_name  # Replace the value
            arguments.append(arg_name)  # Add to the list of arguments, prefix with filter

    # Replace all the filters in the tree with their placeholders
    for arg, value in filters.items():
        tree_str = tree_str.replace(str(value), prefix + str(arg),
                                    1)  # Replace only one occurrence, in case two filters have same value

    callable_tree = compile(tree_str, context, arguments)

    return callable_tree, filters


def node_gradient_descent(node, X, y, lr: float = 0.01, epochs: int = 10):
    """
    Gradient descent on th
    :param node: Node to compute the partial derivative of its
    output with respect to
    :param X: feature matrix
    :param y: target vector
    :param lr: learning rate (default 0.01)
    :param epochs: maximum number of interactions of the algorithm for a single run
    :return:
    """


# Takes a string representation of a tree, and performs gradient descent on the parameters
def gradient_descent(original_tree, xs, ys, context=None, arguments=None, feature_names=None, epochs=10, lr=0.01):
    tree = deepcopy(original_tree)  # Don't modify original tree

    tree_str = str(tree)

    _, _, labels = gp.graph(tree)

    arguments = list(arguments)  # Make a copy of the arguments, so we don't modify original args

    callable_tree, original_filters = replace_filters_with_args(tree_str, labels, context, arguments)

    keys = sorted(list(original_filters.keys()))  # Make sure the order is correct, should be ascending
    filters = [original_filters[key] for key in keys]  # Retrieve the original filter values

    # Split into validation and training sets
    trainX, trainY, validX, validY = split_and_shuffle(xs, ys)
    batch_size = len(trainX) // 10

    # Whether or not to decay learning rate
    decay = True

    # Flag for when to exit
    finished = False

    # Start with largest loss
    lowest_loss = float("inf")
    # best_filters = list(filters)

    # Number of iterations without improvement to wait before exiting
    patience = epochs // 5  # 20%
    num_iterations_without_improv = 0

    last_loss = lowest_loss

    # Gradient descent
    for i in range(epochs):

        # Learning rate decay, halve every 10 epochs
        if i > 0 and i % 10 == 0:
            lr /= 2

        for batchXs, batchYs in make_batches(trainX, trainY, batch_size):

            # args = dict()
            # for name in arguments:
            #     if name in feature_names:
            #         args[name] = batchXs[:, feature_names.index(name)]
            #
            # for k in keys:
            #     args[f"Filter{k}"] = original_filters[k]

            args = tools.args_format(arguments, feature_names, original_filters, batchXs)

            # Compute changes
            grad_fn = grad(compute_loss,
                           3)  # Compute derivative w.r.t the filters - Third index is the filters, see line below
            deltas = grad_fn(callable_tree, batchXs, batchYs, filters, args)
            deltas = np.asarray(deltas)

            # Apply changes
            filters -= (lr * deltas)

            args = tools.args_format(arguments, feature_names, original_filters, validX)

            # Calculate loss on validation set
            validation_loss = compute_loss(callable_tree, validX, validY, filters, args)

            # If this is the lowest loss we've seen, save the filters
            if validation_loss < lowest_loss:
                lowest_loss = validation_loss
                best_filters = list(filters)

            # If we are getting worse on the validation set, record how many epochs this has occurred
            if validation_loss > last_loss:
                num_iterations_without_improv += 1
            else:
                num_iterations_without_improv = 0

            # if we haven't improved for patience epochs, then exit
            if num_iterations_without_improv >= patience:
                filters = best_filters
                finished = True
                break

            last_loss = validation_loss

        if finished:
            break

    # Need to modify the tree to have the new filter values - Need to do it in the this fashion to preserve the order
    for idx, key in enumerate(keys):
        tree[key] = deepcopy(tree[key])  # For some reason deep copy doesn't go DEEP, so need to do it again
        tree[key].value = filters[idx]

    return tree
