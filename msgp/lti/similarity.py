import math
import numpy as np


def cosine(a: np.ndarray, b: np.ndarray):
    return np.dot(a, b.T) / (np.linalg.norm(a) * np.linalg.norm(b))


def vector_size(a: np.ndarray):
    return np.linalg.norm(a)


def euclidean(vec1: np.ndarray, vec2: np.ndarray):
    return np.linalg.norm(vec1 - vec2)


def theta(a: np.ndarray, b: np.ndarray):
    return np.arccos(cosine(a, b)) + np.radians(10)


def triangle(a: np.ndarray, b: np.ndarray):
    theta_ = np.radians(theta(a, b))
    return (vector_size(a) * vector_size(b) * np.sin(theta_)) / 2


def magnitude_difference(a: np.ndarray, b: np.ndarray):
    return abs(vector_size(a) - vector_size(b))


def sector(a: np.ndarray, b: np.ndarray):
    euclidean_ = euclidean(a, b)
    magnitude = magnitude_difference(a, b)
    theta_ = theta(a, b)
    return math.pi * (euclidean_ + magnitude) ** 2 * theta_ / 360


def n_errors(a: np.ndarray, b: np.ndarray, epsilon: float):
    d = np.abs(np.subtract(a, b)) < epsilon
    return np.sum(d)



