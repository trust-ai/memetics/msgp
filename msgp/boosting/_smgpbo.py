from typing import Optional

import numpy as np
from sklearn.utils import Bunch

from msgp import tools
from msgp.lti import SemanticRegressor


class SemanticBoosting(object):
    """
    A Memetic Semantic Boosting for Symbolic Regressor. The weak learners are the
    Memetic Semantic for Genetic Programming (MSGP).
    """

    def __init__(self, boosting_stages: int, **kwargs):
        """
        Create a Semantic Boosting Regressor using the given parameters.
        Args:
            boosting_stages: number of boosting to be used
            **kwargs: arguments required by the Memetic Semantic GP Regressor
        """
        self._boosting_stages = boosting_stages
        _kwargs = kwargs.copy()
        boosters = [SemanticRegressor(**kwargs)] * boosting_stages

        for i in range(boosting_stages):
            _kwargs['random_state'] = i if 'random_state' not in _kwargs else _kwargs['random_state'] + i
            boosters[i] = SemanticRegressor(**_kwargs)

        self._kwargs = _kwargs
        self._boosters = boosters
        self._pset = self._boosters[0].get_pset()
        self._toolbox = self._boosters[0].get_toolbox()
        self._tree = None
        self._tree_feature_names = None

    def fit(self, X, y, **args):
        """
        Train each weak leaner using the fitness cases (X,y)
        Args:
            X: array-like, shape = [n_samples, n_features]
            Training vectors, where n_samples is the number of samples and
            n_features is the number of features.
            y: y: array-like, shape = [n_samples] target values
            **args: arguments used by the regressors
        Returns: returns self
        """

        for booster in self._boosters:
            booster.fit(X, y, **args)
            y = np.round(np.subtract(y, np.nan_to_num(booster.predict(X))), 2)

        boosting_tree = tools.join_trees(self._boosters, self._pset, self._toolbox)

        self._tree = Bunch(epoch=None,
                           tree=boosting_tree,
                           height=boosting_tree.height,
                           length=len(boosting_tree),
                           fitness=boosting_tree.fitness.values[0],
                           coefficients=boosting_tree.coefficients,
                           error=None,
                           residuals=boosting_tree.residuals,
                           rademacher_complexity=None)

        self._tree_feature_names = tools.terminal_names(self._tree.tree)

        return self

    def predict(self, X) -> np.ndarray:
        """
        Perform regression on test vectors X.
        Args:
            X: array-like, shape = [n_samples, n_features]
            Input vectors, where n_samples is the number of samples
            and n_features is the number of features.

        Returns:
            y : array, shape = [n_samples]
            Predicted values for X.
        """
        if self._tree is None:
            raise Exception("You must call fit before predict!")

        predictions = np.zeros(X.shape[0])

        for booster in self._boosters:
            predictions = np.add(predictions, booster.predict(X))

        return predictions

    def get_tree(self):
        return self._tree

    def get_terminal_names(self):
        return self._tree_feature_names

    def get_boosting_stages(self):
        return self._boosting_stages

    # @property
    def get_boosters(self):
        return self._boosters

    def get_expression(self) -> Optional[str]:
        """
        Returns the symbolic expression of the program. It combines the tree
        of each individual booster if a learned was already trained. Otherwise,
        returns None.

        Returns: a symbolic expression of the program. None if the fit function was not
        yet called.
        """
        if self._tree is not None:
            return tools.get_tree_expression(self._tree)
        return None

    def get_tree_expression(self) -> Optional[str]:
        """
        Returns a simplified symbolic expression of the program or None if one was not yet trained.
        Returns: a simplified version of the symbolic expression of the program. None if the fit function was not
        yet called.
        """
        if self._tree is not None:
            return tools.get_tree_expression(self._tree, simplify=True)
        return None
