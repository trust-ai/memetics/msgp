# def compute_rademacher_complexity(y_hat, m: int, rademacher_vector, num_samples: int = 20):
#
#     mid_range = (min(y_hat) - max(y_hat)) / 2
#     h = list([1 if x > mid_range else -1 for x in y_hat])
#
#     complexity = []
#     for s in range(num_samples):
#         random_vector = rademacher_vector[s]
#         correlations = []
#         for i in range(m):
#             correlation = 0 if h[i] == random_vector[i] else 1
#             correlations.append(correlation)
#         complexity.append(sum(correlations))
#     h_complexity = sum(complexity) * (1/num_samples) * (1/m)
#
#     return h_complexity
