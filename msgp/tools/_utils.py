import random
import sys
from typing import List, Dict, Tuple, Sequence, Optional

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from deap import base, gp, creator
from networkx.drawing.nx_agraph import graphviz_layout, write_dot
# from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_absolute_percentage_error
from sklearn.utils import Bunch

__reverse_operators = dict({
    "add": "sub",
    "sub": "add",
    "mul": "div",
    "div": "mul",
    "aq": "mul"
})


def get_reverse_operator(op: gp.Primitive) -> gp.Primitive:
    _rev_op = __reverse_operators[op.name]
    return gp.Primitive(_rev_op, op.args, op.ret)


def terminal_names(expr) -> List[str]:
    args = []
    for i, node in enumerate(expr):
        if isinstance(node, gp.Terminal):
            args.append(node.value)
    return list(set(args))  # set to avoid duplicated name


def feature_indices(variables: List[str], feature_names: List[str]) -> List[int]:
    return [feature_names.index(var) for var in variables]


def args_format(variables, feature_names, filters, data) -> Dict:

    args = dict()
    for name in variables:
        if name in feature_names:
            args[name] = data[:, feature_names.index(name)]

    for k in filters:
        args[f"Filter{k}"] = filters[k]

    return args


def eval_operator(expr, context, **kwargs):

    args = ",".join(arg for arg in kwargs.keys())
    code = "lambda {args}: {code}".format(args=args, code=str(expr))

    try:
        func = eval(code, context, {})
        return func(**kwargs)
    except MemoryError:
        _, _, traceback = sys.exc_info()
        raise MemoryError("Error in tree evaluation :"
                          " Python cannot evaluate a tree higher than 90. "
                          "To avoid this problem, you should use bloat control on your "
                          "operators.").with_traceback(traceback)


def local_error(y_true: np.ndarray, y_pred: np.ndarray) -> float:
    # from scipy.stats import wasserstein_distance
    # mse = mean_squared_error(y_true, y_pred)
    # mae = mean_absolute_error(y_true, y_pred)
    # mape = mean_absolute_percentage_error(y_true, y_pred)
    # wd = wasserstein_distance(y_true, y_pred)

    _, _, mse = linear_scaling(y_true, y_pred)
    return mse


def gp_compile(expr, context):
    code = str(expr)
    names = terminal_names(expr)

    if len(names) > 0:
        args = ",".join(arg for arg in names)
        code = "lambda {args}: {code}".format(args=args, code=code)

    try:
        return eval(code, context, {})
    except MemoryError:
        _, _, traceback = sys.exc_info()
        raise MemoryError("DEAP : Error in tree evaluation :"
                          " Python cannot evaluate a tree higher than 90. "
                          "To avoid this problem, you should use bloat control on your "
                          "operators. See the DEAP documentation for more information. "
                          "DEAP will now abort.").with_traceback(traceback)


def compute_node_outputs(nodes: List[Tuple[int, int, Sequence, gp.PrimitiveTree]],
                         toolbox: base.Toolbox) -> None:
    """ Compute the output for each node.

    param nodes: nodes to compute its output values
    param toolbox: toolbox implementing the function that outputs the predictions for
    each node using the available observations.
    return: None
    """
    for node_id, _, _, node in nodes:
        for _, outputs in zip([node], toolbox.map(toolbox.individual_prediction, [node])):
            node.outputs = outputs

    return None


def compute_node_optimal_values(nodes: List[Tuple[int, int, Sequence, gp.PrimitiveTree]], context, y) -> List[Bunch]:

    _id, _, slice_, root = nodes[0]

    if not isinstance(slice_.start, int):
        print("Warning!!")

    root.optimal = y
    root.error = local_error(root.outputs, root.optimal)

    _, edges, _ = gp.graph(root)
    dct = dict((i, node) for i, _, _, node in nodes)

    ns = [Bunch(id=_id, parent=None, slice_=slice, node=root)]

    for node_id, parent_id, slice_, node in nodes[1:]:
        sibling_id = list(filter(None, set([b if a == parent_id and b != node_id else None for a, b in edges])))
        sibling = dct[sibling_id[0]]
        parent = dct[parent_id]
        op = get_reverse_operator(parent.root)

        expr = creator.Individual([op, gp.Terminal("parent", str, op.ret), gp.Terminal("sibling", str, np.ndarray)])
        node.optimal = eval_operator(expr,
                                     context,
                                     **dict({'parent': parent.optimal, 'sibling': sibling.outputs}))
        node.optimal = node.optimal
        node.error = local_error(node.outputs, node.optimal)

        n = Bunch(id=node_id, parent=dct[parent_id], slice_=slice_, node=node)
        ns.append(n)

    return ns


def get_subtrees(tree: gp.PrimitiveTree, toolbox: base.Toolbox) -> List[Tuple[int, int, Sequence, gp.PrimitiveTree]]:
    """
    Returns all subtrees of a given tree
    Args:
        tree:
        toolbox:

    Returns: A list of tuples. Each tuple comprises the node_id, parent_id, starting and ending index of the subtree,
    and finally the subtree.
    """

    _, edges, _ = gp.graph(tree)
    subtrees = []

    for i, node in enumerate(tree):
        slice_ = tree.searchSubtree(i)

        if not isinstance(slice_.start, int):
            print("Undefined slice")

        st = creator.Individual(gp.PrimitiveTree(tree[slice_.start:slice_.stop]))
        st.id = i
        parent = next(filter(lambda x: x is not None, list(a if b == i else None for a, b in edges)), None)
        if i > 0 and parent is None:
            print("Error. Every non-root node must have a parent!")
        # evaluate_tree([st], toolbox)
        subtrees.append((i, parent, slice_, st))

    return subtrees


def evaluate_tree(tree, toolbox: base.Toolbox):
    if isinstance(tree[0].root, creator.Individual):
        return

    fitness = toolbox.map(toolbox.evaluate, tree)

    for ind, fit in zip(tree, fitness):
        ind.fitness.values = fit


def plot_tree(tree, ax=None, filename: str = None) -> Optional[nx.Graph]:

    if tree is None:
        return None

    nodes, edges, labels = gp.graph(tree)

    g = nx.Graph()
    g.add_nodes_from(nodes)
    g.add_edges_from(edges)
    pos = graphviz_layout(g, prog="dot")

    #
    # nx.draw(g, pos=graphviz_layout(g), ax=ax)
    nx.draw_networkx_nodes(g, pos, nodes, ax=ax, node_size=800, cmap=plt.cm.Blues)
    nx.draw_networkx_edges(g, pos, ax=ax)
    nx.draw_networkx_labels(g, pos, labels, ax=ax)

    if filename:
        write_dot(g, filename)

    return g


def plot(values, xlabel: str, ylabel: str,
         title: str = None, filename: str = None):

    fig, ax = plt.subplots()

    ax.plot(values)
    ax.set_ylabel(ylabel)
    ax.set_xlabel(xlabel)
    ax.set_title(title)

    if filename:
        fig.savefig(filename)

    return fig, ax


def scatter_plots(signals, labels: List[str], title: str, filename: str = None) -> plt.Figure:
    fig, ax = plt.subplots()

    for sgn, label in list(zip(signals, labels)):
        ax.scatter(np.arange(1, len(sgn) + 1), sgn, label=label)

    ax.set_title(title)
    ax.legend()

    if filename:
        fig.savefig(filename)

    return fig


def protected_division(x1, x2):
    import numpy
    with numpy.errstate(divide='ignore', invalid='ignore'):
        return numpy.where(numpy.abs(x2) > 0.001, numpy.divide(x1, x2), 1.)


def aq(x1, x2, epsilon=1):
    return np.divide(x1, np.sqrt(epsilon + np.power(x2, 2)))


def similar(ind_a, ind_b) -> bool:
    return ind_a.fitness.values[0] == ind_b.fitness.values[0] or \
           ind_a.height == ind_b.height


def linear_scaling(y_true, y_pred):
    t_bar = np.mean(y_true)
    y_bar = np.mean(y_pred)

    if np.sum((y_pred - y_bar) ** 2) == 0:
        return None, None, np.nan

    b = np.sum((y_true - t_bar) * (y_pred - y_bar)) / np.sum((y_pred - y_bar) ** 2)
    a = t_bar - b * y_bar

    mse = (1 / len(y_pred)) * np.sum((a + (b * y_pred) - y_true) ** 2)
    return a, b, mse


def compute_rademacher_complexity(predictions, rademacher_vector, num_samples: int):

    m = len(predictions)
    # mid_range = np.ptp(predictions) / 2
    # h = list([1 if p >= mid_range else -1 for p in predictions])
    h = np.ceil(np.tanh(predictions))

    sample_indexes = random.choices(list(range(len(predictions))), k=num_samples)
    # sample_indexes = np.random.choice(list(range(len(predictions))), num_samples)
    hh = h[sample_indexes]
    # random.sample(list(enumerate(predictions)), num_samples)

    complexity = []
    for s in range(num_samples):
        random_vector = rademacher_vector[s]
        correlation = np.correlate(random_vector, h)
        complexity.append(correlation[0])

    h_complexity = sum(complexity) * (1/num_samples) * (1/m)

    return np.round(h_complexity, 6)


def compute_mic_score(individual, data, predictions, target):

    from minepy import MINE
    mine = MINE(alpha=0.6, c=15)
    scores = dict({})

    for terminal_name in terminal_names(individual):
        mine.compute_score(data[terminal_name], target)
        scores[terminal_name] = mine.mic()


def simplify_expression(expression: str) -> str:
    """
    Simplifies an analytic expression by using SymPy.
    Args:
        expression: expression to be simplified. When None return None.
    Returns: simplified version of the given expression
    """
    # from sympy import sympify
    import sympy
    locals = {
        'sub': lambda x, y: x - y,
        'div': lambda x, y: x / y,
        'mul': lambda x, y: x * y,
        'add': lambda x, y: x + y,
        'neg': lambda x: -x,
        'pow': lambda x, y: x ** y,
    }

    return None if expression is None else str(sympy.sympify(expression, locals=locals))


def get_tree_expression(tree: Bunch, simplify: bool = False) -> str:
    """
    Return the string representation of the given tree.
    Args:
        tree: Tree to return its string representation
        simplify: When true returns the simplified version of the tree.

    Returns: The analytic expression of the given tree.
    """
    expression = f"{np.round(tree.coefficients[0], 2)} + " \
                 f"{np.round(tree.coefficients[1], 2)} * " \
                 f"{str(tree.tree)}"

    if simplify:
        expression = simplify_expression(expression)

    return expression


def join_trees(trees, p_set: gp.PrimitiveSetTyped, toolbox: base.Toolbox):
    expression = "".join(["add("] * (len(trees) - 1))

    for i in range(len(trees) - 1, -1, -1):
        expression += str(trees[i].get_tree().tree)
        expression += "," if i >= len(trees) - 2 else ")," if i > 0 else ")"

    joined_tree = creator.Individual(gp.PrimitiveTree.from_string(expression, p_set))
    evaluate_tree([joined_tree], toolbox)

    return joined_tree
