from deap import gp, tools
from numpy import ndarray

# from _rademacher import compute_rademacher_complexity
from ._deapfix import get_generation_strategy
from ._utils import *


def tree_prediction(tree, data, feature_names: List[str], pset: gp.PrimitiveSetTyped) -> ndarray:
    func = gp_compile(expr=tree, context=pset.context)
    names = terminal_names(expr=tree)

    args = dict()
    for name in names:
        args[name] = data[:, feature_names.index(name)]

    if len(names) > 0:
        y_pred = func(**args)
    else:
        y_pred = [func]

    return np.asarray(y_pred)


def create_toolbox(p_set: gp.PrimitiveSetTyped, **kwargs) -> base.Toolbox:
    creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMin, outputs=np.array, optimals=np.array)

    toolbox = base.Toolbox()
    g_function = get_generation_strategy(kwargs['generation_function'])

    toolbox.register("expr", g_function, pset=p_set, min_=kwargs['min_height'], max_=kwargs['max_height'])
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    toolbox.register("compile", gp.compile, pset=p_set)
    toolbox.register("individual_prediction", tree_prediction)

    return toolbox


def create_primitive_set_typed(name: str, feature_names: List[str]) -> gp.PrimitiveSetTyped:
    """
    Create a primitive set to solve a Strongly Typed GP problem.
    The set also defined the researched function return type,
    and input arguments type and number.

    Args:
        name: name of the primitive type
        feature_names: name of the features

    Returns: a primitive set with the defined primitives
    """
    p_set = gp.PrimitiveSetTyped(name, [ndarray] * len(feature_names), ndarray)

    p_set.addPrimitive(np.add, [ndarray, ndarray], ndarray, "add")
    p_set.addPrimitive(np.subtract, [ndarray, ndarray], ndarray, "sub")
    p_set.addPrimitive(np.multiply, [ndarray, ndarray], ndarray, name="mul")
    # p_set.addPrimitive(tools.protected_division, [ndarray, ndarray], ndarray, name="div")
    p_set.addPrimitive(aq, [ndarray, ndarray], ndarray, name="div")

    args = dict()
    if feature_names is not None:
        args_ = {"ARG" + str(i): name for i, name in enumerate(feature_names)}
        for i, name in enumerate(feature_names):
            args["ARG" + str(i)] = name
        p_set.renameArguments(**args)

    return p_set
