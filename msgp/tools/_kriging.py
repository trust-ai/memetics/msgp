import numpy as np
from gstools.krige import Ordinary
from gstools import Gaussian, Exponential


def kriging(X, y_true, y_pred):
    """

    Args:
        X:
        y_true:
        y_pred:

    Returns:

    """
    Gaussian(dim=X.shape[1], var=np.var(y_true))
    model = Exponential(dim=X.shape[1], var=np.var(y_true))
    krig = Ordinary(model, cond_pos=X, cond_val=y_true)

    return krig(y_pred)
